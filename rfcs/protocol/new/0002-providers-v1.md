# RFC: Providers v1 #

| Meta           | Data                       |
|----------------|----------------------------|
| Author         | vados <vados@vadosware.io> |
| Collaborators  |                            |
| Creation date  | 10/08/2020                 |
| Approved by/at |                            |

## Overview ##

`Provider`s are at the heart of Buhzaar -- they constitute the things you can provision/create/run/control with Buhzaar. Defining a flexible yet structured interface is key.

## Revision History ##

- 10/08/2020 - created

## Goals ##

< TODO >

## Non-Goals ##

< TODO >

## Background ##

See `README.md`, and the statements on the goals of Buhzaar.

## Dependencies ##

`Provider`s do not depend on any other concept.

## Prior art ##

Kuberentes has a concept for [Controllers](https://kubernetes.io/docs/concepts/architecture/controller/) which manage `Resource`s. Before the popularization of the Operator pattern controllers were arguably considered more of an internal concept. Buhzaar is at an advantage here in that we do not have to *pivot* to the `Provider`/`Resource`/`Manager` model, we're already there.

## Design ##

< TODO >

## User Experience ##

< TODO >

## Security concerns / Risks ##

< TODO >
