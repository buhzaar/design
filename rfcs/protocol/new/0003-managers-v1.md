# RFC: Managers v1 #

| Meta           | Data                       |
|----------------|----------------------------|
| Author         | vados <vados@vadosware.io> |
| Collaborators  |                            |
| Creation date  | 10/08/2020                 |
| Approved by/at |                            |

## Overview ##

`Manager`s are at the heart of Buhzaar -- they represent the control loops that run to inspect, manage, and manipulate the state of a `Resource` managed by a Buhzaar `Provider`. Defining a flexible yet structured interface is key.

## Revision History ##

- 10/08/2020 - created

## Goals ##

- Allow for independently specified `Manager`s
- Allow finer tuned availability of resource management versus provisioning (a `Provider` may provision a `Resource`, but `Management` may be performed by another entity)
< TODO >

## Non-Goals ##

< TODO >

## Background ##

See `README.md`, and the statements on the goals of Buhzaar.

## Dependencies ##

`Manager`s must specify one or more `Resource`s that it manages. While it is not necessary for any instances of the specified `Resource` to exist, one or more must be specified.

## Prior art ##

Kuberentes has a concept for [Controllers](https://kubernetes.io/docs/concepts/architecture/controller/) which manage `Resource`s. While controllers provide/instantiate *and* provision resources in Kubernetes, the role of `Manager` is intentionally kept separate in Buhzaar.

## Design ##

< TODO >

## User Experience ##

< TODO >

## Security concerns / Risks ##

< TODO >
