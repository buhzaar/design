# `buhzaar` - the Buhzaar Command Line Interface (CLI) #

`buhzaar` is a sub-command of the `buhzaar` binary that allows administration access to an existing Buhzaar system.

Alternatively stated -- when run, `buhzaar cli` interprets it's command line arguments and environment in order to issue commands to a Buhzaar system.
