# `buhzaar-server` - The Buhzaar Server #

`buhzaar-server` is a binary that runs a single instance of the Buhzaar.

Alternatively stated -- when run, `buhzaar-server` attempts to start a functional Buhzaar system with one with a single `Provider`, whose job is to provide and mediate *access* to *other* `Provider`s.
