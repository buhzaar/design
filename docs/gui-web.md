# `buhzaar-gui-web` - The Buhzaar Graphical User Interface for the Web #

`buhzaar-gui-web` is a binary that instantiates and allows access to a graphical user interface for administering a Buhzaar system, via the web.

Alternatively stated -- when run, `buhzaar-web` interprets it's command line arguments and environment in order to make available a website (whether locally or remotely) that allows users to graphically administer a Buhzaar system.
