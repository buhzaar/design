# Buhzaar Design #

This repository contains the thoughts, hopes, dreams, and expectations of those who spend time developing Buhzaar.

## What is Buhzaar? ##

Buhzaar is meant to be an orchestration system that allows for the provision and management of resources.

In allegorical terms, it is a "provider of providers". In Buhzaar's own terms, `buhzaar` itself is a `ProviderProvider` that does the work necessary to provide and mediate *access* to other `Provider`s and `Manager`s, so that clients communicating with `buhzaar` may request `Resource`s from the same or other instances of `buhzaar`. `buhzaar` itself operates on an intentionally meta level -- it treats `Provider`s as a `Resource` and both "provides" and "manages" them.

The definitions of "Provider" and "Resource" and "Manager" are left intentionally vague to allow flexibility in *what* a Buhzaar system can provide and *how* it can undertake provision and management. Administrators may provide and mediate access to network ports, memory, compute, or fully managed applications with a Buhzaar system just as easily as they may provide a single linux container or speak to robots interacting with the real world.

Buhzaar aims to be:

- **Simple** - Buhzaar should be simple to both understand and operate. While a long chain of interactions may be necessarily complex, each individual chain should be as simple and predicable as reasonably possible compared to the related domain.
- **Composable** Buhzaar's pieces (`Provider`s and `Resource`s) should be easy to connect together.
- **Backwards compatible** - Buhzaar and it's components "should never break user space"

Buhzaar is [free/"libre" software](https://en.wikipedia.org/wiki/Free_software), developed in the open.

### Buhzaar the system ###

As mentioned in the previous section, Buhzaar *the system* consists of three simple concepts: `Provider`s, `Resource`s and `Manager`s. `Provider`s make `Resources`s available which are managed by `Manager`s.

The intricacies of how a `Resource` is provisioned, delivered, instantiated, etc are all at the discretion of the relevant `Provider`. The intricacies of how a `Resource` is managed is up to the relevant `Manager`(s). `Provider`s are often also `Manager`s but they may not be.

Buhzaar, and providers written in the same spirit seek to do the minimum possible and enforce reasonable defaults for configurations of `Resource`s, striving to do things that *do not surprise users*.

### `buhzaar` the binary ###

`buhzaar` the binary has three main roles:

- Server (`buhzaar-server`)
- Command line interface to a running buhzaar instance (`buhzaar-cli`)
- Web interface to a running buhzaar instance (`buhzaar-gui-web`)

For more information on each role, see `docs/server.md`, `docs/cli.md` and `docs/gui-web.md` respectively.

## Governance ##

Buhzaar should be governed by a BDFL or relatively small group of "steering" members (likely picked by the BDFL at will). The BDFL and/or steering members are subject to change, and **parties that do not agree with decisions made by either of these entities are encouraged to fork buhzaar** and create something of even more value to the community. Ideas worth pursuing will withstand fragmentation in implementation.

This governance model stands in stark contrast with those taken by other projects that involve creating many committees and bureaucracy -- while that model has it's merits, I find most of the truly venerable long lasting open source projects (like Linux and Postgres) have done well without such a structure. Furthermore, I suspect that governance with such bureaucratic structures (that essentially mirror real political systems) tend to oligarchy by organizations with the most involvement in the bureaucratic structure.

## How does Buhzaar compare/interoperate to <x> ? ##

### Kubernetes ###

Whereas [Kubernetes](https://kubernetes.io/) can be thought of as [the Cathedral](https://en.wikipedia.org/wiki/The_Cathedral_and_the_Buhzaar), Buhzaar is intentionally named named to be the dual of the Catheral -- it is developed in a distributed manner. Attitude wise, a Buhzaar is a scrappier, seedier, more dangerous, far more expansive, exciting, and engaging place than a Cathedral. Buhzaar aims to maintain this relationship with Kubernetes. What Buhzaar lacks in polish and bureaucracy, it means to make up in unfettered opportunity, rapid iteration, and chaos.

In more practical terms, Kubernetes bills itself as a "container orchestrator", but Buhzaar aims to orchestrate *anything it can*. Since Buhzaar runs on computers (today), that means that it likely will be orchestrating containerized (or not) compute workloads of some sort, but this may change as the developers and users of Buhzaar will it.

### Linux ###

[Linux](https://en.wikipedia.org/wiki/Linux) is the predominant server operating system that Buhzaar was developed to run on top of. In it's original design, Buhzaar is not an operating system, though it may one day be interpreted as one. Buhzaar sits on top of Linux.
